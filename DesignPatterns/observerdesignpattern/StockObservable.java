package observerdesignpattern;

public interface StockObservable {
	
	public void add(NotificationAlertObserver observer);
	
	public void remove(NotificationAlertObserver observer);
	
	public void notifySubcribers();
	
	public void setStockCount(int newStockCount);
	
	public int stockCount();

}
