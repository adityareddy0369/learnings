package observerdesignpattern;

public class EmailAlertObserver implements NotificationAlertObserver {
	
	public StockObservable observable; // we can use this observable object to know which item is in stock
	public String emailId; 
	
	public EmailAlertObserver(StockObservable observable, String emailId) {
		this.observable = observable;
		this.emailId = emailId;
	}

	@Override
	public void update() {
		sendEmail(emailId,  "your product is in stock");
	}

	private void sendEmail(String emailId, String msg) {
		System.out.println("mail sent to " + emailId + " with msg " + msg);
	}
	
}
