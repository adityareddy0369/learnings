package observerdesignpattern;

public class MobileAlertObserver implements NotificationAlertObserver {

	public StockObservable observable; // we can use this observable object to know which item is in stock
	public String userName; 
	
	public MobileAlertObserver(StockObservable observable, String userName) {
		this.observable = observable;
		this.userName = userName;
	}

	@Override
	public void update() {
		sendMobileNotification(userName,  "your product is in stock");
	}

	private void sendMobileNotification(String userName, String msg) {
		System.out.println("mobile notification sent to " + userName + " with msg " + msg);
	}

}
