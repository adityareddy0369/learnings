package observerdesignpattern;

import java.util.ArrayList;
import java.util.List;

public class IphoneStockObservable implements StockObservable {
	
	public List<NotificationAlertObserver> ObserverList = new ArrayList<>();
	
	public int stockCount = 0;

	@Override
	public void add(NotificationAlertObserver observer) {
		ObserverList.add(observer);
	}

	@Override
	public void remove(NotificationAlertObserver observer) {
		ObserverList.remove(observer);
	}

	@Override
	public void notifySubcribers() {
		for(NotificationAlertObserver observer: ObserverList) {
			observer.update();
		}
	}

	@Override
	public void setStockCount(int newStockCount) {
		if(stockCount == 0) {
			notifySubcribers();
		}
		stockCount += newStockCount;
	}

	@Override
	public int stockCount() {
		return stockCount;
	}
	

}
