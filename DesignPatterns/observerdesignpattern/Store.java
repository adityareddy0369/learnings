package observerdesignpattern;

public class Store {
	public static void main(String[] args) {
		StockObservable iphoneObservable = new IphoneStockObservable();
		
		NotificationAlertObserver observer1 = new EmailAlertObserver(iphoneObservable, "asd@gmail.com");
		NotificationAlertObserver observer2 = new MobileAlertObserver(iphoneObservable, "asd_name");
		
		iphoneObservable.add(observer1);
		iphoneObservable.add(observer2);
		
		iphoneObservable.setStockCount(10);
	}
}
