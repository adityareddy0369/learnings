Windows Eclipse:
ctrl+shift+R : to open class, interface, enums
ctrl+O: to find method in a class
ctrl+H: search a word in project
alt+left key: last edited place
f6: next line debug
f8: next breakpoint debug
---------------------------------------------------------------------------------------------------------------------------------------
Mac Intellij:
shift+shift: to open methods, class, enums, interfaces
command+shift+F: search a word in project
command+shift+leftkey: last edited place
f8: next line debug
command+option+left key: last edited place
---------------------------------------------------------------------------------------------------------------------------------------
windows:
ctrl+c: copy
ctrl:x: cut
ctrl:v: paste
ctrl:A: select all
fn+right key: to move to end of the sentence
fn+shift+right key: to select till end of the sentence
windows+D: desktop page
---------------------------------------------------------------------------------------------------------------------------------------
mac:
command+c: copy
command+v: paste
command+shift+f: find and replace in sublime text
f11: desktop page




