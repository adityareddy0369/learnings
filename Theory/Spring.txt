The Spring framework can be considered as a collection of sub-frameworks, also referred to as layers, such as 
Spring AOP, Spring ORM, Spring Web Flow, and Spring Web MVC. You can use any of these modules separately while constructing a Web application.
 The modules may also be grouped together to provide better functionalities in a web application.

Prior to penetrating down to Spring to container do remember that Spring provides two types of Containers namely as follows:

BeanFactory Container
ApplicationContext Container
The features of the Spring framework such as IoC, AOP, and transaction management, make it unique among the list of frameworks.
 Some of the most important features of the Spring framework are as follows:

IoC container
Data Access Framework
Spring MVC
Transaction Management
Spring Web Services
JDBC abstraction layer
Spring TestContext framework
Spring IoC Container is the core of Spring Framework. It creates the objects, configures and assembles their dependencies, manages their entire life cycle.
 The Container uses Dependency Injection(DI) to manage the components that make up the application.
It gets the information about the objects from a configuration file(XML) or Java Code or Java Annotations and Java POJO class.
These objects are called Beans. Since the Controlling of Java objects and their lifecycle is not done by the developers, hence the name Inversion Of Control.
Spring uses java reflection api to create objects which are specified in configuration file as beans. 

-----------------------------------------------------------------------------------------------------------------------------------------
1. Spring MVC is often regarded as the most modern representation of monolithic architecture.
2. In Microservices each service can have different models. In that a service can have mvc model too.
3. In Spring Web MVC, the DispatcherServlet class works as the front controller.
   DispatcherServlet is responsible to manage the flow of the Spring MVC application.
4. https://www.youtube.com/watch?v=8dXZZBCFjwk&ab_channel=SeleniumExpress

-----------------------------------------------------------------------------------------------------------------------------------------
Spring Aop:(aspect oriented programming)
Can change xml files instead of changing the methods
concepts: aspect, advice, joinpoints, pointcut

cross cutting concerns:
The cross-cutting concern is a concern which is applicable throughout the application. This affects the entire application.
For example, logging, security and data transfer are the concerns needed in almost every module of an application, thus they are the cross-cutting concerns
These cross-cutting concerns are handled by the aspect unit.
If we  write an aspect class, it can be applicable in the entire application 

aspect:Module of code for a cross cutting concern(logging, security..)
advice:what action to be taken and when it should be applied
join point: when to apply the code during program execution
point cut: A predicate expression for where advice should be applied

Weaving:
connecting aspect and target objects(can happend at compile time, run time/load time) 

