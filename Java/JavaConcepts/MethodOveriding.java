package Javaconcepts;

class A3 {
	
	public void show() {
		System.out.println("Inside A");
	}
}

class B3 extends A3 {
	
	public void show() {
		super.show();
		System.out.println("Inside B");
	}
}
public class MethodOveriding {

	public static void main(String[] args) {
		B3 b = new B3();
		b.show();
	}

}
