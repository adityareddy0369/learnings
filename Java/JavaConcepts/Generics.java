package Javaconcepts;

import java.util.ArrayList;

// Generics doesn't support primitive types

//class Containter<T> {
class Containter<T extends Number> {
	T value;

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
	
	public void show() {
		System.out.println(value.getClass().getName());
	}
	
	//public void show1(ArrayList<? extends Number> list) { }
	public void show1(ArrayList<? extends T> list) { }
	public void show2(ArrayList<? super T> list) { }
}
public class Generics {

	public static void main(String[] args) {
		//Containter<?> con = new Containter<>();
		
		Containter<Number> con = new Containter<>();
		Containter<Integer> con1 = new Containter<>();
		con.value = 2;
		con1.value = 3;
		con.show();
		
		//con.show1(new ArrayList<String>()); //  compile time error
		con.show1(new ArrayList<>()); 
		con.show1(new ArrayList<Integer>());
		
		// con1.show1(new ArrayList<Number>()); // compile time error
		con1.show2(new ArrayList<Number>());
		
	}

}
