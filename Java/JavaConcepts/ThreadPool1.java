package Javaconcepts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Task implements Runnable {

	 private String name;
     
	    public Task(String s)
	    {
	        name = s;
	    }
	      
	    // Prints task name and sleeps for 1s
	    // This Whole process is repeated 5 times
	    public void run()
	    {
	        try
	        {
	            for (int i = 0; i<=5; i++)
	            {
	                if (i==0)
	                {
	                    Date d = new Date();
	                    SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
	                    System.out.println("Initialization Time for"
	                            + " task name - "+ name +" = " +ft.format(d));   
	                    //prints the initialization time for every task 
	                }
	                else
	                {
	                    Date d = new Date();
	                    SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
	                    System.out.println("Executing Time for task name - "+
	                            name +" = " +ft.format(d));   
	                    // prints the execution time for every task 
	                }
	                Thread.sleep(1000);
	            }
	            System.out.println(name+" complete");
	        }
	          
	        catch(InterruptedException e)
	        {
	            e.printStackTrace();
	        }
	    }
	
}
public class ThreadPool1 {

	public static void main(String[] args) {
		
		// Creating five new tasks
		Runnable task1 = new Task("task 1");  
		Runnable task2 = new Task("task 2");  
		Runnable task3 = new Task("task 3");  
		Runnable task4 = new Task("task 4");  
		Runnable task5 = new Task("task 5"); 
		ExecutorService executor = Executors.newFixedThreadPool(3);
		
		executor.execute(task1);
		executor.execute(task2);
		executor.execute(task3);
		executor.execute(task4);
		executor.execute(task5);
		
		executor.shutdown();
	}

}
