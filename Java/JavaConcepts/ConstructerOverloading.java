package Javaconcepts;

class Cal {
	int i = 3;
	int j = 8;
	String s = "A";
	
	 Cal(int i, int j){
		this.i = i;
		this.j = j;
	}
	 Cal(int i, String s) {
		this.i = i;
		this.s = s;
	}
}
public class ConstructerOverloading {

	// can override main method also but jvm will always call main with array of string parameter method
	public static void main(String[] args) {
		Cal c = new Cal(4, 6);
		Cal c1 = new Cal(4, "B");
	}

}
