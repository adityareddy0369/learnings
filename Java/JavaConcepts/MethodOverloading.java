package Javaconcepts;

class Calculator {
	public void add(int a, int b) {
		System.out.println(a+b);
	}
	public void add(int a, double b) {
		System.out.println(a+b);
	}
	public void add(int a, int b, int c) {
		System.out.println(a+b+c);
	}
}

public class MethodOverloading {

	public static void main(String[] args) {
		Calculator c = new Calculator();
		c.add(3, 1);
		c.add(9, 2.5);
		c.add(5, 8, 1);
	}

}
