package Javaconcepts;

public class Singleton {
	
	public static void main(String[] args) {
		Thread t1 = new Thread(new Runnable() {

			@Override
			public void run() {
				A obj1 = A.getInstance();
			}
		});

		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				A obj2 = A.getInstance();
			}
		});
		
		Thread t3 = new Thread(() -> {
			A obj3 = A.getInstance();
		});
		
		t1.start();
		t2.start();

	}
}

class A {
	
	// static variables will be initialized during class loading but instance variables will be initialized during instance creation
	// static A obj = new A(); // Eager instantiation(Even if we are not using, it will create)
	static A obj;
	// private constructor ensures that no object of this class is created outside this class
	private A() {
		System.out.println("Instance created");
	}

	public static A getInstance() {
		// Double checked locking
		if (obj == null) { // outer if is used to prevent redundant locks
			synchronized (A.class) { // if the method is not static, we can use synchronized(this) { }
				if (obj == null) {
					obj = new A();
				}
			}
		}

		return obj;
	}
}



