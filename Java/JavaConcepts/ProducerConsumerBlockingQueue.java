package Javaconcepts;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ProducerConsumerBlockingQueue {

	private static BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(1);

	public static void main(String[] args) {

		Thread producerThread = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 1; i <= 10; i++) {
					try {
						//Thread.sleep(1000);
						queue.put(i);
						System.out.println("Produced" + i);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		Thread consumerThread = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 1; i <= 10; i++) {
					try {
						System.out.println("consumer" + queue.take());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		});

		producerThread.start();
		consumerThread.start();

	}

}
