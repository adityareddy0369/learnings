package Javaconcepts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
// https://www.youtube.com/watch?v=oAp4GYprVHM&ab_channel=Telusko
class Employee implements Comparable<Employee>{
	private int id;
	private String name;
	private String email;
	
	public Employee(int id, String name, String email) {
		this.id = id;
		this.name = name;
		this.email = email;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", email=" + email + "]";
	}
	@Override
	public int compareTo(Employee emp) {
		if(this.id > emp.id) {
			return 1;
		} else {
			return -1;
		}
	}
	
}
public class ComparableComparator {

	public static void main(String[] args) {
		Employee emp1 = new Employee(122, "E", "d@gmail.com");
		Employee emp2 = new Employee(534, "G", "d@gmail.com");
		Employee emp3 = new Employee(634, "A", "z@gmail.com");
		Employee emp4 = new Employee(534, "A", "a@gmail.com");
		Employee emp5 = new Employee(534, "Z", "a@gmail.com");
		
		List<Employee> list = new ArrayList<>();
		list.add(emp1);
		list.add(emp2);
		list.add(emp3);
		list.add(emp4);
		list.add(emp5);
		
		Employee[] arr = {emp1, emp2, emp3};
		Arrays.sort(arr);
		
		for(Employee emp: arr) {
			System.out.println(emp);
		}
		
		System.out.println();
		
		Collections.sort(list);
		for(Employee emp: list) {
			System.out.println(emp);
		}
		System.out.println();
		Comparator<Employee> com = new Comparator<Employee>() {
			@Override
			public int compare(Employee emp1, Employee emp2) {
				if(emp1.getId() < emp2.getId()) {
					return 1;
				} else {
					return -1;
				}
			}
		};
		
		Comparator<Employee> com1 = new Comparator<Employee>() {
			@Override
			public int compare(Employee emp1, Employee emp2) {
				return emp1.getName().compareTo(emp2.getName());
			}
		};
		
		Comparator<Employee> com2 = new Comparator<Employee>() {
			@Override
			public int compare(Employee emp1, Employee emp2) {
				 if(emp1.getId() ==  emp2.getId()) {
					return emp1.getName().compareTo(emp2.getName());
				 } else if(emp1.getId() >  emp2.getId()) {
					 return 1;
				 } else {
					 return -1;
				 }
			}
		};
		
		// Even if we have comparable logic, we can overdide it with comparator object
		Collections.sort(list, com2);
		for(Employee emp: list) {
			System.out.println(emp);
		}
		System.out.println();
		
		Comparator<Employee> compare = Comparator.comparing(Employee::getId).thenComparing(Employee::getName);
		list.stream().sorted(compare).forEach(System.out::println);
		list.stream().sorted(Comparator.comparing(Employee::getId).thenComparing(Employee::getName)).forEach(System.out::println);
	}

}
