package Javaconcepts;

class A1 extends Thread {
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("A");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class B1 extends Thread {
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("B");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

public class Multithreading {

	public static void main(String[] args) {
		A1 obj1 = new A1();
		B1 obj2 = new B1();

		obj1.start();
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		obj2.start();
	}

}
