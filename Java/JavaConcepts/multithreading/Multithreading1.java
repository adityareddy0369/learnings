package Javaconcepts;

class A2 extends Thread {
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("A");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class B2 extends Thread {
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("B");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

public class Multithreading1 {

	public static void main(String[] args) {

		// Runnable obj1 = new A1();
		// Runnable obj2 = new B2();

		A1 obj1 = new A1();
		B1 obj2 = new B1();

		Thread t1 = new Thread(obj1);
		Thread t2 = new Thread(obj2);

		t1.start();
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		t2.start();
	}

}
