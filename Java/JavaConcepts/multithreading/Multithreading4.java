package Javaconcepts;

public class Multithreading4 {

	public static void main(String[] args) throws Exception {

		Thread t1 = new Thread(() -> {
			for (int i = 0; i < 5; i++) {
				System.out.println("A");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		Thread t2 = new Thread(() -> {
			for (int i = 0; i < 5; i++) {
				System.out.println("B");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		t1.start();
		Thread.sleep(10);
		t2.start();
		
		System.out.println("Is t1 alive: " + t1.isAlive());
		t1.join();
		t2.join();
		System.out.println("Is t1 alive: " + t1.isAlive());
		
		System.out.println("Main thread");
	}

}
