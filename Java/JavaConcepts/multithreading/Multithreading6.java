package Javaconcepts;

class X {
	private int num;
	boolean isValueSet = false;

	public synchronized void put(int num) {
		while (isValueSet) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Put" + num);
		this.num = num;
		isValueSet = true;
		notify();
	}

	public synchronized void get() {
		while (!isValueSet) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Get" + num);
		isValueSet = false;  
		notify();
	}

}

class Producer implements Runnable {
	X x;

	Producer(X x) {
		this.x = x;
		Thread t1 = new Thread(this, "Producer");
		t1.start();
	}

	public void run() {
		int i = 0;
		while (true) {
			x.put(i++);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class Consumer implements Runnable {
	X x;

	Consumer(X x) {
		this.x = x;
		Thread t2 = new Thread(this,"Consumer");
		t2.start();
	}

	public void run() {
		while (true) {
			x.get();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}

public class Multithreading6 {

	public static void main(String[] args) {
		X x = new X();
		new Producer(x);
		new Consumer(x);
	}

}
