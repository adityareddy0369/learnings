package Javaconcepts;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class WorkerThread implements Runnable {
	private String message;  
    public WorkerThread(String s){  
        this.message=s;  
    }  
	@Override
	public void run() {
			System.out.println(Thread.currentThread().getName()+" (Start) message = "+message);  
	        processmessage();//call processmessage method that sleeps the thread for 2 seconds  
	        System.out.println(Thread.currentThread().getName()+" (End)");//prints thread name  
	}

	private void processmessage() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
public class ThreadPool {

	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(5); //creating a pool of 5 threads  
		//WorkerThread thread = new WorkerThread();
		for(int i = 0; i< 10; i++) {
			Runnable thread = new WorkerThread(i + " ");
			executor.execute(thread);
		}
		executor.shutdown();  
        while (!executor.isTerminated()) {   }  
  
        System.out.println("Finished all threads");  
    }  
}
