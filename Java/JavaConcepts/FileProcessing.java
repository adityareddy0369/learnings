package Javaconcepts;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileProcessing {

	public static void main(String[] args) throws IOException {
		// Bufferd writer and reader are wrapper classes for file writer and reader
				FileWriter writter = new FileWriter("D:/Aditya/newFile1.txt");
				//writter.write("Aditya");
				BufferedWriter bw = new BufferedWriter(writter);
				bw.write("Aditya");
				bw.newLine();
				bw.write("Aditya1");
				bw.close();
				
				FileReader reader = new FileReader("D:/Aditya/newFile1.txt");
				Scanner sc = new Scanner(reader);
				while(sc.hasNextLine()) {
					System.out.println(sc.nextLine());
				}
				
				try {
					Stream<String> words = Files.lines(Paths.get("D://Aditya/newFile1.txt"));
					words.forEach(System.out::println);
					
					Stream<String> lines = Files.lines(Paths.get("D://Aditya/newFile1.txt"));
					List<String> list = lines.filter(x -> x.endsWith("1")).map(x -> x.toLowerCase()).collect(Collectors.toList());
					System.out.println(list);
				} catch (IOException e) {
					e.printStackTrace();
				}
	}

}
