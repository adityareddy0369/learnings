package txst;

/* (2 pts)Write a recursive function named countV that takes three arguments:
an array of integers and its size and an integer v, and returns a count of the
number of times v appears in the list.
For example, if a contains {9,7,3,4,7,1} then countV(a,6,7) returns 2.
Do not use loops, extra parameters, or global or static variables. */

public class RecursionCount {

	public static void main(String[] args) {
		int[] arr = {9,7,3,7,7};
		System.out.println(countV(arr, arr.length, 7));
		System.out.println(count(arr,arr.length,7));
	}

	private static int count(int[] arr, int size, int v) {
		if(size == 0) {
			return 0;
		}
		if(arr[size-1] == v) {
			return 1+count(arr,size-1,v);
		} else {
			return count(arr,size-1,v);
		}
	}

	private static int countV(int[] arr, int size, int v) {
		int count = 0;
		if(size == 0) {
			return count;
		}
		if(arr[size-1] == v) {
			count = 1 + countV(arr, size-1, v);
		} else {
			count = countV(arr, size-1, v);
		}
		
		return count;
		
	}

}
