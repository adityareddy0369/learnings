package txst;

import java.util.ArrayList;
import java.util.List;

/*(4 pts) Declare and implement a class to represent a list of integers, called
IntList. Use an array of integers of size 100 to store the values in the list.
Use an integer variable named num to store the number of elements currently in
the list. */

/*Include the following functions in your class:
• a no-argument constructor that sets up an empty list.
• a void function add(x) that adds a new value, x, to the end of the list. If
adding the element would exceed the list’s capacity, this function should
output an error message.
• a function sort(a,size) that takes an array of integers (a) and its size and
rearranges the items in the array so that they are in ascending order. You may
use any algorithm to sort the array. It should not change the order of the
elements in the list member.
• A function median() that returns the median value of the list of integers. If
the values are in order from smallest to largest, the median is the middle
value. If the list contains an even number of values, the median is the average
of the two middle values. If the list is empty, the median is 0. This function
should not change the order of the elements in the list member. For example,
if the list contains{3,5,8,4,9,6,7} the median is 6. If the list contains
{6,7,8,9,1,2,3,4} the median is 5. If the list contains {7,8,9,10,1,2,3,4} the
median is 5.5. */
public class IntList {
	int num = 0;
	List<Integer> list;
	
	public IntList() {
		this.list = new ArrayList<Integer>();
	}
	public static void main(String[] args) {
		int[] arr = {3,6,8,1,0};
		int size = arr.length;
		IntList list = new IntList();
		for(int i = 0; i<100; i++) {
			list.add(i);
		}
		list.sort(arr, size);
		System.out.println(list.median());
	}
	private long median() {
		long median = 0;
		int size = list.size();
		if(list.isEmpty()) {
			return 0;
		}
		if(size%2 == 0) {
			median = list.get((size/2 + ((size/2)-1))/2);
		} else {
			median = list.get(size/2);
		}
		return median;
		
	}
	private void sort(int[] arr, int size) {
		boolean isSwapped;
		for(int i= 0; i< size - 1; i++) {
			isSwapped = false;
			for(int j = 0; j< size-1-i; j++) {
				if(arr[j] > arr[j+1]) {
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
					isSwapped = true;
				}
			}
			if(!isSwapped) {
				return;
			}
		}
	}
	private void add(int x) {
		if(num > 99) {
			throw new IndexOutOfBoundsException();
			}
		list.add(100-num, x);
		num++;
	}

}
