package txst;

/*(2 pts) Write a function named validate that takes an array of integers and a
size as an argument. The function should return true if ALL of the numbers are
between 0 and 100 (inclusive). If ANY of the numbers in the array is less than
0 or greater than 100, the function should return false.
For example, for the array containing {0,3,8,100} with size 4 it should return
true. For the array containing {0,99,101,77,87} with size 5 it should return
false (because 101 is greater than 100). */

public class Inclusive {

	public static void main(String[] args) {
		int[] arr = {0,99,101,77,87};
		System.out.println(validate(arr, arr.length));
	}

	private static boolean validate(int[] arr, int size) {
		for(int i= 0; i< size; i++) {
			if(arr[i] < 0 || arr[i] > 100) {
				return false;
			}
		}
		return true;
		
	}
}
