package txst;

/*(2 pts) Define the public member function removeLast() that removes the last
element from the linked list. If the list is empty, the function should do nothing
(it should not cause an error, it should not output an error message). */

public class RemoveLastLinkedList {
	private  Node head;
	
	private class Node {
		private int data;
		private Node next;
	}
	
	public RemoveLastLinkedList() {
		head = null;
	}

	public static void main(String[] args) {
		RemoveLastLinkedList list = new RemoveLastLinkedList();
		list.removeLast();
	}

	private void removeLast() {
		if(head == null) {
			return;
		}
		
		Node current = head;
		Node previous = null;
		if(current.next != null) {
			previous = current;
			current = current.next;
		}
		previous.next = null;
	}

}
