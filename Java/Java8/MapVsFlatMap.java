package Java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Customer {
	int id;
	String name;
	String mail;
	List<String> phoneNums;

	public Customer(int id, String name, String mail, List<String> phoneNums) {
		super();
		this.id = id;
		this.name = name;
		this.mail = mail;
		this.phoneNums = phoneNums;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public List<String> getPhoneNums() {
		return phoneNums;
	}

	public void setPhoneNums(List<String> phoneNums) {
		this.phoneNums = phoneNums;
	}
}

class CustomerTable {

	public static List<Customer> getData() {
		return Stream
				.of(new Customer(1, "A", "a@gmail.com", Arrays.asList("1111111111", "2222222222")),
						new Customer(2, "B", "b@gmail.com", Arrays.asList("3333333333", "4444444444")),
						new Customer(3, "C", "c@gmail.com", Arrays.asList("5555555555", "6666666666")))
				.collect(Collectors.toList());
	}
}

public class MapVsFlatMap {

	public static void main(String[] args) {
		List<Customer> customers = CustomerTable.getData();
		List<String> emails = customers.stream().map(x -> x.getMail()).collect(Collectors.toList());
		System.out.println(emails);
		List<List<String>> phoneNums = customers.stream().map(x -> x.getPhoneNums()).collect(Collectors.toList());
		System.out.println(phoneNums);
		List<String> phoneNums1 = customers.stream().flatMap(x -> x.getPhoneNums().stream())
				.collect(Collectors.toList());
		System.out.println(phoneNums1);

	}

}
