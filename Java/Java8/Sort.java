package Java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class User {
    
    private String name;
    private int age;
    
    public User(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", age=" + age + "]";
	}

}

public class Sort {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(3,5,6,1,4,0,9);
		
		System.out.println(list.stream().sorted().collect(Collectors.toList()));
		
		System.out.println(list.stream().sorted(Collections.reverseOrder()).collect(Collectors.toList()));
		
		List<User> userList = new ArrayList<>(Arrays.asList(
		        new User("John", 33), 
		        new User("Robert", 26), 
		        new User("Mark", 26), 
		        new User("Brandon", 42)));
		
		System.out.println(userList.stream().sorted(Comparator.comparing(User::getName)).collect(Collectors.toList()));
		System.out.println(userList.stream().sorted(Comparator.comparingInt(User::getAge)).collect(Collectors.toList()));
		
		
	}

}
