package Java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Streams1 {
	
	Employee employee;

	public static void main(String[] args) {
		//List<String> list = Arrays.asList("a", "b", "c");
		List<Integer> list = new ArrayList<>();
		for(int i = 0; i< 30; i++) {
			list.add(i);
		}
		for(int i = 0; i< list.size(); i++) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		for(int i : list) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		list.forEach(System.out::print);
		System.out.println();
		
		list.forEach(i -> System.out.print(i + " "));
		System.out.println();
		
		list.parallelStream().forEach(i -> System.out.print(i)); // parallelStream uses multiple treads to print the list values
		System.out.println();
		
		List<Integer> evenList = list.stream().filter(i -> i%2 == 0).collect(Collectors.toList());
		evenList.forEach(i -> System.out.print(i + " "));
		System.out.println();
		
		list.stream().map(i -> i+2); //  intermediate method cannot print the values
		
		list.stream().map(i -> i*2).forEach(i -> System.out.print(i + " ")); //  only terminary methods can print the values
		System.out.println();
		
		list.forEach(i -> System.out.print(i + " "));
		System.out.println();
		
		List<Employee> employeeList = new ArrayList<>();
		employeeList.add(new Employee(1, "a"));
		employeeList.add(new Employee(2, "B"));
		employeeList.add(new Employee(3, "c"));
		employeeList.add(new Employee(4, "D"));
		
		// using 2 filters will work as a and operator
		Optional<Employee> employee = employeeList.stream().filter(s -> "B".equals(s.getName())).filter(i -> i.getId() == 2).findAny();
		if(employee.isPresent()) System.out.println(employee);
		
		Optional<Employee> employee1 = employeeList.stream().filter(e -> "B".equals(e.getName()) || e.getId() == 3).findFirst();
		if(employee1.isPresent()) System.out.println(employee1.get().getName());
		
		// most of the the time findAny returns the first element but there is no guarantee that it does
		Optional<Employee> employee2 = employeeList.stream().filter(e -> e.getId() == 3 || "B".equals(e.getName())).findAny();
		if(employee2.isPresent()) System.out.println(employee2);
		
		Employee employee3 = employeeList.stream().filter(e -> "Z".equals(e.getName())).findAny().orElse(null);
		if(employee3 == null) System.out.println(employee3);
		
		Employee employee4 = employeeList.stream().filter(e -> "B".equals(e.getName())).findAny().orElse(null);
		if(employee4 != null) System.out.println(employee4.getName());
		
		// to deal with primitive types
		int[] array = {1,2,3,4};
		int[] newArray = IntStream.of(array).filter(i -> i%2 == 0).toArray();
		System.out.println(Arrays.toString(newArray));
		
		// flat map is used to convert 2 level stream into 1 level stream or 2d arrray into 1 d array
		Integer[][] array2d = {{1,2}, {3,4},{5,6}};
		//Stream<Object> stream = Arrays.stream(array2d).flatMap(Stream::of);
		//int[] arr =stream.mapToInt(Integer::intValue).toArray();	
		  List<Integer> arr = Arrays.stream(array2d).flatMap(x -> Arrays.stream(x)).collect(Collectors.toList());
		  System.out.println(arr);
	}
	
	// for inner class declare the class as static or create a instance of the main class and use that ref to access the inner class
	 static class Employee {
			private int id;
			private String name;
			private String title;
			private int salary;
			
			public Employee(int id, String name) {
				super();
				this.id = id;
				this.name = name;
			}
			
			public Employee(int id, String name, String title, int salary) {
				super();
				this.id = id;
				this.name = name;
				this.title = title;
				this.salary = salary;
			}
			
			public int getId() {
				return id;
			}
			public void setId(int id) {
				this.id = id;
			}
			public String getName() {
				return name;
			}
			public void setName(String name) {
				this.name = name;
			}
			
			
			public String getTitle() {
				return title;
			}

			public void setTitle(String title) {
				this.title = title;
			}

			@Override
			public String toString() {
				return "Employee [id=" + id + ", name=" + name + ", title=" + title + ", salary=" + salary + "]";
			}

			public int getSalary() {
				return salary;
			}

			public void setSalary(int salary) {
				this.salary = salary;
			}
	 }
}
