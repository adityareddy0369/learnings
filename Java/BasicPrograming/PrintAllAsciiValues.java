package BasicPrograming;

// 48-57(0-9), 65-90(A-Z), 97-122(a-z)
// to get a ascii number of a character sum that character with 0
public class PrintAllAsciiValues {

	public static void main(String[] args) {
		for(int i = 0; i< 256; i++) {
			System.out.print((char)i + " ");
		}
	}

}
