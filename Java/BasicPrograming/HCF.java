package BasicPrograming;

import java.util.Scanner;

public class HCF {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter first number: ");
		int n1 = sc.nextInt();
		System.out.println("Enter second number: ");
		int n2 = sc.nextInt();
		hcfOfTwoNumbers(n1, n2);

	}

	private static void hcfOfTwoNumbers(int n1, int n2) {
		int lcm = lcmOfTwoNumbers(n1, n2);
		// product of two numbers = lcm*hcf
		System.out.println((n1*n2)/lcm);
	}
	
	private static int lcmOfTwoNumbers(int n1, int n2) {
		int high, low;
		if(n1>n2) {
			high = n1;
			low = n2;
		} else {
			high = n2;
			low = n1;
		}
		int i = 1;
		while(true) {
			if((high*i)%low == 0) {
				return high*i;
			}
			i++;
		}
	}
		

}
