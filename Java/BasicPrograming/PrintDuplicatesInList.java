package BasicPrograming;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PrintDuplicatesInList {

	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(4,2,1,3,4,2,9);
		Set<Integer> set = new HashSet<>();
		for(int i = 0; i< list.size(); i++) {
			//set.add returns boolean false if the element we are trying to add already exists
			if(set.add(list.get(i)) == false) { 
				System.out.println(list.get(i));
			}
		}
	}

}
