package BasicPrograming;

public class Factorial {

	public static void main(String[] args) {
		System.out.println(factorialLoop(5));
		System.out.println(factorialRecursion(5));
	}

	private static int factorialLoop(int n) {
		int fact = 1;
		for(int i = 1; i < 6; i++) {
			fact = fact*i;
		}
		return fact;
	}

	private static int factorialRecursion(int n) {
		if (n <= 1) {
			System.out.println("factorial of (1) = 1");
			return 1;
		}
		System.out.println("factorial of (" + n + ") = " + n + " * factorial of (" + (n-1) + ")");
		return n * factorialRecursion(n - 1);
	}

}
