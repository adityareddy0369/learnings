package BasicPrograming;

//Sum of each digit which is multiplied by itself n times should be equal to the actual integer(n is the length of the given integer)
public class ArmstrongNum {

	public static void main(String[] args) {
		// 153, 407, 1634
		int n = 1634;
		System.out.println(isArmStrongNum(n));
	}

	private static boolean isArmStrongNum(int n) {
		int temp = n;
		int rem = 0, length = 0, sum = 0;
		while(temp != 0) {
			temp = temp/10;
			length++;
		}
		temp = n;
		while(temp != 0) {
			rem = temp%10;
			sum  = (int) (sum + Math.pow(rem, length));
			temp = temp/10;
		}
		if(n == sum) {
			return true;
		} else {
			return false;
		}
	}

}
