package BasicPrograming;

public class Patterns {

	public static void main(String[] args) {
		int n = 5;
		upTraingle(n);
		downTraingle(n);
		bottomRightTraingle(n);
		bottomLeftTraingle(n);
		topRightTraingle(n);
		topLeftTraingle(n);
	}

	private static void topLeftTraingle(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}

	}

	private static void topRightTraingle(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print("  ");
			}
			for (int k = 0; k < n - i; k++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	private static void bottomLeftTraingle(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < i + 1; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	private static void bottomRightTraingle(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - 1 - i; j++) {
				System.out.print("  ");
			}
			for (int k = 0; k < i + 1; k++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	private static void downTraingle(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(" ");
			}
			for (int k = 0; k < n - i; k++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

	private static void upTraingle(int n) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - 1 - i; j++) {
				System.out.print(" ");
			}
			for (int k = 0; k < i + 1; k++) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}

}
