package BasicPrograming;

public class Fibonacci {

	public static void main(String[] args) {
		printNFiboNumsForLoop(10);
		System.out.println();
		printNFiboNumsWhileLoop(10);
		System.out.println();
		System.out.print(printNthFibNumRecursion(10));
		System.out.println();
		printTillNFiboNums(100);
	}

	private static void printTillNFiboNums(int n) {
		int firstTerm = 0, secondTerm = 1, sum = 0;
		while(firstTerm <= n) {
			System.out.print(firstTerm + " ");
			sum = firstTerm + secondTerm;
			firstTerm = secondTerm;
			secondTerm = sum;
		}
	}
	
	private static int printNthFibNumRecursion(int n) {
		if(n <= 1) {
			return n;
		}
		return printNthFibNumRecursion(n-1) + printNthFibNumRecursion(n-2);
	}

	private static void printNFiboNumsWhileLoop(int n) {
		int firstTerm = 0, secondTerm = 1, sum = 0, i = 0;
		while(i < n) {
			System.out.print(firstTerm + " ");
			sum = firstTerm + secondTerm;
			firstTerm = secondTerm;
			secondTerm = sum;
			i++;
		}
	}

	private static void printNFiboNumsForLoop(int n) {
		int firstTerm = 0;
		int secondTerm = 1;
		int sum = 0;
		for (int i = 0; i < n; i++) {
			System.out.print(firstTerm + " ");
			sum = firstTerm + secondTerm;
			firstTerm = secondTerm;
			secondTerm = sum;
		}
	}

}
