package BasicPrograming;

public class IsPalindrome {

	public static void main(String[] args) {
		System.out.println(isIntPalindrome("161"));
		System.out.println(isStringPalindrome("ada"));
	}

	// Take 2 pointers, one from start and one from end and compare
	private static boolean isStringPalindrome(String string) {
		int i = 0, j = string.length() - 1;
		while(i < j) {
			if(string.charAt(i) != string.charAt(j)) return false;
			i++;
			j--;
		}
		return true;
	}

	// Remove each digit from last and form complete number and compare with actual number
	private static boolean isIntPalindrome(String string) {
		int num = Integer.parseInt(string);
		int rem, sum = 0, temp = num;
		while(num > 0) {
			rem = num%10;
			sum = (sum*10) + rem;
			num = num/10;
		}
		if(sum == temp) {
			return true;
		} else {
			return false;
		}
	}

}
