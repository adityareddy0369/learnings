package interview;

public class SecondLargestElement {
	public static void main(String[] args) {
		int[] arr = {4,8,7,9,2,6,9};
		System.out.println(findLargestElement(arr));
		System.out.println(findSecondLargestElement1(arr));
		System.out.println(findSecondLargestElement2(arr));
	}

	private static int findSecondLargestElement2(int[] arr) {
		// time: O(n); space: O(1)
		// one traversal
		int first = 0, second = 0;
		for(int i=1; i<arr.length; i++) {
			if(arr[i] > arr[first]) {
				second = first;
				first = i;
			} else if(arr[i] > arr[second] & arr[i] != arr[first]) {
				second = i;
			}
		}
		return arr[second];
	}

	private static int findSecondLargestElement1(int[] arr) {
		// time: O(n); space: O(1)
		// two traversal
		int maxIndex = 0;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > arr[maxIndex]) {
				maxIndex = i;
			}
		}
		int secondMaxIndex = 0;
		for(int j=1; j<arr.length; j++) {
			if(arr[j] > arr[secondMaxIndex] && arr[j] != arr[maxIndex]) {
				secondMaxIndex = j;
			}
		}
		return arr[secondMaxIndex];
	}

	private static int findLargestElement(int[] arr) {
		// time: O(n); space: O(1)
		int maxIndex = 0;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > arr[maxIndex]) {
				maxIndex = i;
			}
		}
		return arr[maxIndex];
	}
	
}
