package interview;

import java.util.Stack;

public class ImplementQueueUsingStack {
	static Stack<Integer> stack;
	static Stack<Integer> temp;
	static int top;

	public ImplementQueueUsingStack() {
		stack = new Stack<>();
		temp = new Stack<>();
	}
	
	public static void main(String[] args) {
		int x = 10;
		push(x);
		pop();
		peek();
		empty();
	}

	private static void push(int x) {
		if(stack.empty()) {
			top = x;
		}
		stack.push(x);
	}

	private static int pop() {
		while(!stack.empty()) {
			temp.push(stack.pop());
		}
		int popped = temp.pop();
		if(!temp.empty()) {
			top = temp.peek();
		}
		while(!temp.empty()) {
			stack.push(temp.pop());
		}
		return popped;
	}

	private static int peek() {
		return top;
	}

	private static boolean empty() {
		return stack.empty();
	}
}
