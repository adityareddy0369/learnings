package interview;

import java.util.ArrayList;
import java.util.List;

// given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
// Input:  A = [1, 3, 6, 4, 1, 2] , output: 5
// Input:  A = [-1, -3], output: 1
public class BaseBack1 {

	public static void main(String[] args) {
		//int[] arr = {1,3,6,4,1,2};
		int[] arr = {-1, -3};
		List<Integer> list = new ArrayList<>();
		for(int i = 0; i< arr.length; i++) {
			list.add(arr[i]);
		}
		int j = 1;
		while(true) {
			if(!list.contains(j)) {
				System.out.println(j);
				break;
			}
			j++;
		}
	}

}
