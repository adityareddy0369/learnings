package interview;

import java.io.CharConversionException;
import java.util.Arrays;

public class Anagram {

	static int totalChars = 256;
	
	public static void main(String[] args) {
		String s1 = "traingle";
		String s2 = "integral";
		System.out.println(areAnagramSortingtechnique(s1, s2));
		System.out.println(areAnagram(s1.toCharArray(), s2.toCharArray()));
	}


	private static boolean areAnagramSortingtechnique(String s1, String s2) {
		char[] str1 = s1.toCharArray();
		char[] str2 = s2.toCharArray();
		
		Arrays.sort(str1);
		Arrays.sort(str2);
		String newS1 = new String(str1);
		String newS2 = new String(str2);
		if(newS1.equals(newS2)) return true;
		
		return false;
	}

	private static boolean areAnagram(char[] str1, char[] str2) {
		if(str1.length != str2.length) return false;
		
		int[] count1 = new int[totalChars];
		Arrays.fill(count1, 0);
		int[] count2 = new int[totalChars];
		Arrays.fill(count2, 0);
		
		for(int i = 0; i< str1.length; i++) {
			count1[str1[i]]++;
			count2[str2[i]]++;
		}
		
		for(int i = 0; i< totalChars; i++)
			if(count1[i] != count2[i]) return false;
		
		return true;
	}

}
