package interview;

import java.util.HashSet;

public class FindTripletsWithSumZero {

	public static void main(String[] args) {
		int[] arr = { 0, -1, 2, -3, 1 };
		findTripletsNaiveApproach(arr);
		System.out.println();
		findTriplets(arr);
		System.out.println();
		findTriplets1(arr);
	}

	private static void findTriplets1(int[] arr) {
		// Time complexity: quadratic: 0(n^2), Auxiliary space: constant: 0(1)
		// link: https://www.geeksforgeeks.org/find-triplets-array-whose-sum-equal-zero/
		
	}

	private static void findTriplets(int[] arr) {
		// Time complexity: quadratic: 0(n^2), Auxiliary space: linear: 0(n)
		int n = arr.length;
		for(int i = 0; i < n-1; i++) {
			HashSet<Integer> set = new HashSet<>();
			for(int j = i+1; j< n; j++) {
				int x = -(arr[i] + arr[j]);
				if(set.contains(x)) {
					System.out.println(x + " " + arr[i] + " " + arr[j]);
				} else {
					set.add(arr[j]);
				}
			}
		}
	}

	private static void findTripletsNaiveApproach(int[] arr) {
		// Time complexity : cubic: 0(n^3), Auxiliary space : constant: 0(1)
		int n = arr.length;
		for (int i = 0; i < n - 2; i++) {
			for (int j = i + 1; j < n - 1; j++) {
				for (int k = j + 1; k < n; k++) {
					if (arr[i] + arr[j] + arr[k] == 0) {
						System.out.println(arr[i] + " " + arr[j] + " " + arr[k]);
					}
				}
			}
		}

	}

}
