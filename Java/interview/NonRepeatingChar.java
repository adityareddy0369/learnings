package interview;

import java.util.HashMap;
import java.util.Map;

public class NonRepeatingChar {
	

	public static void main(String[] args) {
		String str = "geeksforgeeks";
		nonRepeatingChar(str);
		nonRepeatingChar1(str);
	}

	// using hashmap
	private static void nonRepeatingChar1(String str) {
		Map<Character, Integer> map = new HashMap<>();
		for(int i = 0; i< str.length(); i++) {
			map.put(str.charAt(i), map.getOrDefault(str.charAt(i), 0)+1);
		}
		for(int i = 0; i< str.length(); i++) {
			if(map.get(str.charAt(i)) == 1) {
				System.out.println(str.charAt(i));
				break;
			}
		}
	}

	// using 256 char array
	private static void nonRepeatingChar(String str) {
		char[] count = new char[256];

		for (int i = 0; i < str.length(); i++) {
			count[str.charAt(i)] += 1;
		}
		for (int i = 0; i < str.length(); i++) {
			if (count[str.charAt(i)] == 1) {
				System.out.println(str.charAt(i));
				break;
			}
		}
	}

}
