package interview;

//Number of flips to make binary string alternate
// Input: str = “0001010111”, output: 2
public class BaseBack2 {

	public static void main(String[] args) {
		String str = "0001010111";
		System.out.println(Math.min(result(str, '0'), result(str, '1')));
		
	}

	private static int result(String str, char expected) {
		int flipCount = 0;
		for(int i = 0; i< str.length(); i++) {
			if(str.charAt(i) != expected) {
				flipCount++;
			}
			expected = flip(expected);
		}
		return flipCount;
	}

	private static char flip(char expected) {
		if(expected == '0') {
			expected = '1';
		} else if(expected == '1'){
			expected = '0';
		}
		return expected;
	}

}
