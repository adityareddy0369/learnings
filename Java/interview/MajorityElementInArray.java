package interview;

import java.util.HashMap;
import java.util.Map;

public class MajorityElementInArray {

	public static void main(String[] args) {
		int[] arr = {3, 3, 4, 2, 3, 3, 3, 4, 2, 4, 4};
		findMajority(arr);
		findMajority1(arr);
		
		
	}

	// moore voting algoritham
	private static void findMajority1(int[] arr) {
		int count = 0;
		int result = 0;
		for(int i = 0; i< arr.length; i++) {
			if(count == 0) {
				result = arr[i];
			}
			if(arr[i] == result) {
				count += 1;
			} else {
				count -= 1;
			}
		}
		System.out.println(result);
	}

	private static void findMajority(int[] arr) {
		int element = 0;
		int highestCount = 0;
		Map map = new HashMap();
		for(int i = 0; i< arr.length; i++) {
			int count = (int) map.getOrDefault(arr[i], 1);
			map.put(arr[i], count);
			if(count > highestCount) {
				highestCount = count;
				element = arr[i];
			}
		}
		System.out.println(element);
	}

}
