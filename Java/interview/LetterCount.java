package interview;

import java.util.HashMap;
import java.util.Map;

public class LetterCount {
	public static void main(String[] args) {
		StringBuilder sb = new  StringBuilder();
		String str = "aaahhcccddaah";
		Map<Character, Integer> map = new HashMap();
		for(int i = 0; i< str.length(); i++) {
			int value = (int) map.getOrDefault(str.charAt(i), 1);
			map.put(str.charAt(i), value+1);
		}
		System.out.println(map);
		for(Map.Entry<Character, Integer> entry : map.entrySet()) {
			sb.append(entry.getKey());
			sb.append(entry.getValue());
		}
		System.out.println(sb);
	
	}

}
