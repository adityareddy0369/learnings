package interview;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FirstNonRepeatingCharUsingJava8 {

	public static void main(String[] args) {
		 String inputStr ="teeter";
		 // First and store the chars and it's count in linkedhashMap
		 Map<Character, Long> linkedHashMap = inputStr.chars()
				 .mapToObj(c -> (char)c)
				 .collect(Collectors.groupingBy(Function.identity(), LinkedHashMap::new, Collectors.counting()));
		 System.out.println(linkedHashMap);
		 Optional<Character> result = linkedHashMap.entrySet().stream().filter(v -> v.getValue() == 1).map(k -> k.getKey()).findFirst();
		 if(result.isPresent()) {
			 System.out.println(result.get());
		 } else {
			 System.out.println("no non repeating char found");
		 }
	}

}
