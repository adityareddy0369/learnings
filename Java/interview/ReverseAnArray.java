package interview;

import java.util.Arrays;

public class ReverseAnArray {

	public static void main(String[] args) {
		int[] arr = {5,2,6,2,6,3};
		System.out.println(Arrays.toString(reverse(arr)));
		reverseWhileLoop(arr);
	}

	private static void reverseWhileLoop(int[] arr) {
		int i = 0;
		int j = arr.length-1;
		while(i<j) {
			arr[i] = arr[i] + arr[j];
			arr[j] = arr[i] - arr[j];
			arr[i] = arr[i] - arr[j];
			i++;
			j--;
		}
		System.out.println(Arrays.toString(arr));
	}

	private static int[] reverse(int[] arr) {
		// time: O(n); space: O(1)
		int size = arr.length;
		for(int i=0; i< size/2; i++) {
			arr[i] = arr[i] + arr[size-1-i];
			arr[size-1-i] = arr[i]-arr[size-1-i];
			arr[i] = arr[i]-arr[size-1-i];
		}
		return arr;
	}

}
