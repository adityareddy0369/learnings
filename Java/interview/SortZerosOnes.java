package interview;
import java.util.Arrays;

public class SortZerosOnes {

	public static void main(String[] args) {
		int[] arr = {1,0,0,0,1,0,0};
		int[] arr1 = {2,1,1,0,1,0,2,1};
		int[] arr2 = {1,0,0,0,1,0,0};
		int[] arr3 = {1,0,0,0,1,0,0};
		sortcounter(arr);
		sortCounter1(arr1);
	}

	
	private static void sortcounter(int[] arr) {
		int zerosCount = 0;
		for(int i = 0; i< arr.length; i++) {
			if(arr[i] < 1) {
				zerosCount++;
			} 
		}
		for(int i = 0; i< zerosCount; i++) {
			arr[i] = 0;
		}
		for(int i = zerosCount; i< arr.length; i++) {
			arr[i] = 1;
		}
		System.out.println(Arrays.toString(arr));
	}
	
	private static void sortCounter1(int[] arr1) {
		int zerosCount = 0, onesCount = 0;
		for(int i = 0; i< arr1.length; i++) {
			if(arr1[i] < 1) {
				zerosCount++;
			} else if(arr1[i] == 1) {
				onesCount++;
			}
		}
		for(int i = 0; i< zerosCount; i++) {
			arr1[i] = 0;
		}
		for(int i = zerosCount; i< onesCount; i++) {
			arr1[i] = 1;
		}
		for(int i = onesCount; i< arr1.length; i++) {
			arr1[i] = 2;
		}
		System.out.println(Arrays.toString(arr1));
	}

}
