package interview;

import java.util.HashMap;
import java.util.Map;

public class Capgemini2 {

	public static void main(String[] args) {
		String s = "aabbbbeeeeffggg";
		result(s);
	}
	
	private static void result(String s) {
		Map<Character, Integer> map = new HashMap<>();
		for(int i = 0; i< s.length(); i++) {
			map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
		}
		for(Map.Entry<Character, Integer> entrySet : map.entrySet()) {
			String result = String.valueOf(entrySet.getKey()) + entrySet.getValue();
			System.out.print(result);
		}
	}

}
