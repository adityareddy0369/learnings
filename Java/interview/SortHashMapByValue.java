package interview;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class SortHashMapByValue {
	
	public static void main(String[] args) {
		Map<Integer, Employee> map = new HashMap<>();
		map.put(2, new Employee(3, "D"));
		map.put(1, new Employee(1, "A"));
		map.put(6, new Employee(9, "B"));
		sortByComparator(map);
	}

	private static void sortByComparator(Map<Integer, Employee> map) {
		System.out.println("Original Map:");
		map.forEach((k,v) -> System.out.println(k + " " + v));
		
		//map.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEach(System.out::println);
		System.out.println("Sorted by employee name using streams: ");
		map.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.comparing(Employee::getName))).forEach(System.out::println);
		Set<Entry<Integer, Employee>> entrySet = map.entrySet();
		List<Entry<Integer, Employee>> list = new ArrayList<>(entrySet);
		
		//Collections.sort(list, (o1, o2) -> o1.getValue().getName().compareTo(o2.getValue().getName()));
		
		Collections.sort(list, new Comparator<Entry<Integer, Employee>>() {
			@Override
			public int compare(Entry<Integer, Employee> o1, Entry<Integer, Employee> o2) {
				return o1.getValue().getName().compareTo(o2.getValue().getName());
				//return o1.getValue().getId() - o2.getValue().getId();
			}
		});
		System.out.println("Sorted Map:");
		//System.out.println(list);
		list.forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));
		
	}
}

 class Employee {
	private int id;
	private String name;
	
	public Employee(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + "]";
	}
}
