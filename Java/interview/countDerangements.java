package interview;

public class countDerangements {

	public static void main(String[] args) {
		int n = 4;
		System.out.println(derange(n));
	}

	private static int derange(int n) {
		// base case
		if(n == 1) return 0;
		if(n == 2) return 1;
		
		int ans = (n-1) * (derange(n-1) + derange(n-2));
		return ans;
	}

}
