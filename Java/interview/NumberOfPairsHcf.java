package interview;

public class NumberOfPairsHcf {

	public static void main(String[] args) {
		int n = 4;
		int pairs = 0;
		for(int i = 1; i< n; i++) {
			for(int j = 1; j< n; j++) {
				pairs += hcf(i,j) == 1 ? 1 : 0;
			}
		}
		System.out.println(pairs);
		System.out.println(pairs/2 + 1); // without repetition of pairs
	}

	private static int hcf(int a, int b) {
		int hcf = 0;
		for(int i = 1; i<=a; i++) {
			if(a%i == 0 && b%i == 0) {
				hcf = i;
			}
		}
		return hcf;
	}

}
