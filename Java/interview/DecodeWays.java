package interview;

public class DecodeWays {

	public static void main(String[] args) {
		String s = "226";
		decodeWays(s);
		decodeWays1(s);
	}

	private static void decodeWays1(String s) {
		 // number of ways for each index will be stored in dp array
        int[] dp = new int[s.length()];
        dp[0] = s.charAt(0) == '0' ? 0 : 1;
        for(int i = 1; i< s.length(); i++) {
            int oneDigit = Integer.valueOf(s.substring(i, i+1));
            int twoDigit = Integer.valueOf(s.substring(i-1, i+1));
            if(oneDigit >= 1) {
                dp[i] += dp[i-1];
            }
            if(twoDigit >= 10 && twoDigit <= 26) {
                if(i-2 >= 0) {
                    dp[i] += dp[i-2];
                } else {
                    dp[i] += dp[i-1];
                }
            }
        }
        System.out.println(dp[s.length()-1]);
		
	}

	//faster
	private static void decodeWays(String s) {
		int[] dp = new int[s.length()+1];
		dp[0] = 1;
		dp[1] = s.charAt(0) == '0' ? 0 : 1;
		for(int i = 2; i<= s.length(); i++) {
			int oneDigit = Integer.valueOf(s.substring(i-1, i));
			int twoDigit = Integer.valueOf(s.substring(i-2, i));
			if(oneDigit >= 1) {
				dp[i] += dp[i-1];
			}
			if(twoDigit >= 10 && twoDigit <= 26) {
				dp[i] += dp[i-2];
			}
		}
		System.out.println(dp[s.length()]);
	}

}
