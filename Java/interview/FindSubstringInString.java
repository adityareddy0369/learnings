package interview;

public class FindSubstringInString {

	public static void main(String[] args) {
		String s = "iampracticing";
		String s1 = "am";
		System.out.println(s.contains(s1));
		System.out.println(s.indexOf(s1) > -1);
		System.out.println(findSubstringInString(s,s1));
		
	}

	private static boolean findSubstringInString(String s, String s1) {
		int j = 0;
		for(int i = 0; i< s.length(); i++) {
			// find first character
			if(s.charAt(i) == s1.charAt(j)) {
				if(s1.length()-1 == j) {
					return true;
				}
				j++;
			} else {
				j = 0;
			}
		}
		return false;
	}

}
