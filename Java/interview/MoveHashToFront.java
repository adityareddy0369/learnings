package interview;

public class MoveHashToFront {

	public static void main(String[] args) {
		String s = "Move#Hash#to#Front";
		moveHashToFront(s);
		moveHashToFront1(s);
	}

	private static void moveHashToFront1(String s) {
		String s1 = new String("");
		String s2 = new String("");
		for(int i = 0; i< s.length(); i++) {
			if(s.charAt(i) == '#') s1 += '#';
			else s2 += s.charAt(i);
		}
		String result = s1+s2;
		System.out.println(result);
	}

	private static void moveHashToFront(String s) {
		StringBuilder sb = new StringBuilder();
		int count = 0;
		for(int i = 0; i< s.length(); i++) {
			if(s.charAt(i) == '#') count++;
			else sb.append(s.charAt(i));
		}
		for(int j = 0; j< count; j++) {
			sb.insert(j, '#');
		}
		System.out.println(sb);
	}

}
