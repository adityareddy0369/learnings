package interview;

public class LongestPalindromeSubString {

	static int startIndex = 0;
	static int endIndex = 0;

	public static void main(String[] args) {
		String str = "forgeeksskeegfor";
		String str1 = "aaaabbaa";
		longestPalindromeSubString(str); //   time: O(n^2) space: O(1) // take a single or double characters, expand and find the range
		longestPalindromeSubString1(str1); // time: O(n^2) space: O(n^2)  Dynamic programming
	}

	public static void longestPalindromeSubString1(String str) {
		int n = str.length();
		String subString = "";
		boolean[][] dp = new boolean[n][n];
		for (int i = 0; i < n; i++) {
			for (int s = 0, e = s + i; e < n; s++, e++) { // start & end index pointers
				if (i == 0) {
					dp[s][e] = true;
					subString = str.substring(s, e + 1);
				} else if (i == 1) {
					if (str.charAt(s) == str.charAt(e)) {
						dp[s][e] = true;
						subString = str.substring(s, e + 1);
					} else {
						dp[s][e] = false;
					}
				} else {
					if (str.charAt(s) == str.charAt(e)) {
						dp[s][e] = dp[s + 1][e - 1];
						if (dp[s][e] && subString.length() < e - s + 1) {
							subString = str.substring(s, e + 1);
						}
					} else {
						dp[s][e] = false;
					}
				}
			}
		}
		System.out.println(subString);
	}

	private static void longestPalindromeSubString(String str) {
		for (int i = 0; i < str.length() - 1; i++) {
			int b = i, f = i; // backward and forward traversal pointers to expand palindrome range
			expand(str, b, f);
			expand(str, b, f + 1);
		}
		System.out.println(str.substring(startIndex, endIndex + 1));
	}

	private static void expand(String str, int b, int f) {
		while (b >= 0 && f < str.length() && str.charAt(b) == str.charAt(f)) {
			b--;
			f++;
		}
		if (endIndex - startIndex < (f - 1) - (b + 1)) {
			startIndex = b + 1;
			endIndex = f - 1;
		}

	}
}
