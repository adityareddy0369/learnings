package interview;
// https://www.youtube.com/watch?v=SVFXEqn3Ceo&ab_channel=Pepcoding
public class Capgemini3 {

	public static void main(String[] args) {
		int[][] arr = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 }, { 17, 18, 19, 20 } };
		int row = 5;
		int column = 4;
		result(arr, row, column);
	}

	private static void result(int[][] arr, int row, int column) {
		int minRow = 0;
		int minCol = 0;
		int maxRow = row - 1;
		int maxCol = column - 1;
		while (maxCol > minCol && maxRow > minRow) {
			// right traverse
			for (int i = minCol; i<= maxCol; i++) {
				System.out.print(arr[minRow][i] + " ");
			}
			minRow++;
			// down traverse
			for(int i = minRow; i<= maxRow; i++) {
				System.out.print(arr[i][maxCol]+ " ");
			}
			maxCol--;
			// left traverse
			for(int i = maxCol; i>= minCol; i--) {
				System.out.print(arr[maxRow][i] + " ");
			}
			maxRow--;
			// up traverse
			for(int i = maxRow; i>= minRow; i--) {
				System.out.print(arr[i][minCol] + " ");
			}
			minCol++;
		}

	}

}
