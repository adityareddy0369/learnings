package interview;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class ThreadClass implements Runnable {

	@Override
	public void run() {
		synchronized (ThreadClass.class) {
			for(int i = 0; i< 5; i++) {
				System.out.print("Thread1 ");
				System.out.print("Thread2 ");
				System.out.print("Thread3 ");
				System.out.println();
			}
		}
	}
	
}

public class Print3ThreadsUsingThreads {
	public static void main(String[] args) {
		ThreadClass obj = new ThreadClass();
		ExecutorService executer = Executors.newFixedThreadPool(3);
		executer.execute(obj);
	}

}
