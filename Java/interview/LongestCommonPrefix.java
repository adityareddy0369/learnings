package interview;
 
// Longest Common Prefix using Sorting
public class LongestCommonPrefix {

	public static void main(String[] args) {
		String[] arr = {"geeksforgeeks", "beeks", "geek", "geezer"};
		System.out.println(longestCommonPrefix(arr));
	}

	private static String longestCommonPrefix(String[] arr) {
		String prefix = arr[0];
		for(int i = 1; i< arr.length; i++) {
			while(arr[i].indexOf(prefix) == -1) {
				prefix = prefix.substring(0, prefix.length()-1);
			}
		}
		return prefix;
	}

}
