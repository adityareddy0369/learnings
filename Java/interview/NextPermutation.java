package interview;

import java.util.Arrays;

public class NextPermutation {

	public static void main(String[] args) {
		int[] arr = {1,3,5,4,2};
		System.out.println(Arrays.toString(nextPermutation1(arr)));
	}

	private static int[] nextPermutation1(int[] arr) {
		int size = arr.length;
		int swapIndex = -1;
		for(int i=0; i<size-1; i++) {
			if(arr[i] < arr[i+1]) {
				swapIndex = i;
			} else {
				continue;
			}
		}
		if(swapIndex != -1) {
			arr[swapIndex] = arr[swapIndex]+arr[swapIndex+1];
			arr[swapIndex+1] = arr[swapIndex]-arr[swapIndex+1];
			arr[swapIndex] = arr[swapIndex]-arr[swapIndex+1];
		} else {
			for(int j=0; j<size/2; j++) {
				arr[j] = arr[j]+arr[size-1-j];
				arr[size-1-j] = arr[j]-arr[size-1-j];
				arr[j] = arr[j]-arr[size-1-j];
			}
		}
		return arr;
		
	}

}
