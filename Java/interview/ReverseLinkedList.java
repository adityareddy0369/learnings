package interview;

public class ReverseLinkedList {
	private static Node head;
	
	private static class Node {
		private int data;
		private Node next;

		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	public static void main(String[] args) {
		insertEnd(1);
		insertEnd(2);
		insertEnd(3);
		insertEnd(4);
		display();
		reverseLinkedList();
		display();
	}
	

	private static void reverseLinkedList() {
		Node current = head;
		Node previous = null;
		while(current != null) {
			head = current.next;
			current.next = previous;
			previous = current;
			current = head;
		}
		head = previous;
	}


	public static void display() {
		Node current = head;
		while(current != null) {
			System.out.print(current.data + "->");
			current = current.next;
		}
		System.out.println("null");
	}
	
	public static void insertEnd(int i) {
		Node lastNode = new Node(i);
		if(head == null) {
			head = lastNode;
			return;
		}
		Node current = head;
		while(current.next != null) {
			current = current.next;
		}
		current.next = lastNode;
	}

}
