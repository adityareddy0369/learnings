package interview;

public class StringPermutation {

	public static void main(String[] args) {
		String str = "abc";
		stringPermutation(str, "");
		System.out.println();
		stringPermutation1(str, "");
		// need to practice non recursive way
	}

	// Recursion with distinct permutations
	private static void stringPermutation1(String str, String ans) {
		if(str.isEmpty()) {
			System.out.print(ans + " ");
			return;
		}
		boolean[] alpha = new boolean[26];
		for(int i = 0; i< str.length(); i++) {
			char ch = str.charAt(i);
			String s = str.substring(0, i) + str.substring(i+1);
			if(alpha[ch - 'a'] == false) {
				stringPermutation1(s, ans+ch);
				alpha[ch - 'a'] = true;
			}
		}
	}

	// Recursion with repetitive permutations
	private static void stringPermutation(String str, String ans) {
		if(str.length() == 0) {
			System.out.print(ans + " ");
			return;
		}
		for(int i = 0; i< str.length(); i++) {
			char ch = str.charAt(i);
			String s = str.substring(0, i) + str.substring(i+1);
			stringPermutation(s, ans+ch);
		}
	}
	
}
