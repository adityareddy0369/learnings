package interview;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcConnection {

	public static void main(String[] args) throws Exception {
		createJdbcConnection();
	}

	private static void createJdbcConnection() throws Exception {
		// create and register the driver
		Class.forName("com.mysql.cj.jdbc.Driver"); //  what ever driver we put in forname method parms , will load at the time of class loading
		String url = "jdbc:mysql://localhost:3306/table_name";
		String userName = "root";
		String password = "root";
		String query = "selet * from Employee";
		
		Connection conn = DriverManager.getConnection(url, userName, password);
		Statement st =  conn.createStatement();
		ResultSet rs = st.executeQuery(query);
		while(rs.next()) {
			String name = rs.getString("name");
		}
		
		st.close();
		conn.close();
		
	}

}
