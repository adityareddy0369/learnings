package Implementation;

// For a binary search tree all the nodes to the left of root is less than root and right of the root is greater than root
// Both the left and right subtrees must also be binary search trees.
public class BinarySearchTreeRecursive {

	private static Node root;

	class Node {
		private int data;
		private Node left;
		private Node right;

		public Node(int data) {
			this.data = data;
		}
	}

	public static void main(String[] args) {
		BinarySearchTreeRecursive bst = new BinarySearchTreeRecursive();
		// Duplicates in binary search tree are not allowed
/*     	 _10_
		|    |
	   _5_   15
	  |	  |
	  3   4                */
		
		bst.insert(10);
		bst.insert(5);
		bst.insert(15);
		bst.insert(3);
		bst.insert(4);
		
		bst.search(3);
		bst.preOrderRecusrsive(root);
	}
	
	public void preOrderRecusrsive(Node root) {
		if(root == null) {
			return;
		}
		System.out.print(root.data + " ");
		preOrderRecusrsive(root.left);
		preOrderRecusrsive(root.right);
	}

	private void search(int key) {
		search(root, key);
	}

	private Node search(Node root, int key) {
		if(root == null || root.data == key) {
			return root;
		}
		if(key < root.data) {
			return search(root.left, key);
		} else {
			return search(root.right, key);
		}
	}

	private void insert(int value) {
		root = insert(root, value);
	}

	private Node insert(Node root, int value) {
		if (root == null) {
			root = new Node(value);
			return root;
		}
		if(value < root.data) {
			root.left = insert(root.left, value);
		} else {
			root.right = insert(root.right, value);
		}
		return root;

	}

}
