package Implementation;

import Implementation.BinarySearchTreeRecursive.Node;

public class BinarySearchTreeIterative {
	private static Node root;

	class Node {
		private int data;
		private Node left;
		private Node right;

		public Node(int data) {
			this.data = data;
		}
	}
	
	public static void main(String[] args) {
		BinarySearchTreeIterative bst = new BinarySearchTreeIterative();
		// Duplicates in binary search tree are not allowed
/*     	 _10_
		|    |
	   _5_   15
	  |	  |
	  3   4                */
		
		bst.insert(10);
		bst.insert(5);
		bst.insert(15);
		bst.insert(3);
		bst.insert(4);
		
		bst.preOrderRecusrsive(root);
	}
	
	public void preOrderRecusrsive(Node root) {
		if(root == null) {
			return;
		}
		System.out.print(root.data + " ");
		preOrderRecusrsive(root.left);
		preOrderRecusrsive(root.right);
	}
		
	private void insert(int data) {
		if(root == null) {
			root = new Node(data);
			return;
		}
		Node current = root;
		while(current != null) {
			if(data < current.data) {
				current = current.left;
			} else {
				current = current.right;
			}
		}
		current = new Node(data);
	}

}
