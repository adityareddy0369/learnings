package Sorting;

import java.util.Arrays;

// Take a element starting from 1st index and place it in the appropriate position in the left side
public class InsertionSort {

	public static void main(String[] args) {
		int[] arr = { 5, 1, 10, 2, 9 };
		
		sortAsc(arr);
		System.out.println(Arrays.toString(arr));
		sortDesc(arr);
		System.out.println(Arrays.toString(arr));
		sort(arr);
		System.out.println(Arrays.toString(arr));
	}

	// take a index and compare with all the left elements
	private static void sort(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			int j = i - 1;
			while (j >= 0 && arr[j + 1] < arr[j]) {
				int temp = arr[j + 1];
				arr[j + 1] = arr[j];
				arr[j] = temp;
				j--;
			}
		}
	}

	private static void sortDesc(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			int temp = arr[i];
			int j = i - 1;
			while (j >= 0 && arr[j] < temp) {
				arr[j + 1] = arr[j];
				j = j - 1;
			}
			arr[j + 1] = temp;
		}
	}

	
	// take elements by looping, insert them in temp and keep comparing with all the left elements 
	private static void sortAsc(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			int temp = arr[i];
			int j = i - 1;
			while (j >= 0 && arr[j] > temp) {
				arr[j + 1] = arr[j]; 
				j--;
			}
			arr[j + 1] = temp;
		}
	}

}
