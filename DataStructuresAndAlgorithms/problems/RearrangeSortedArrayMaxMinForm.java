package problems;

import java.util.Arrays;

public class RearrangeSortedArrayMaxMinForm {

	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5,6,7};
		rearrange(arr);
		System.out.println(Arrays.toString(arr));
	}

	private static void rearrange(int[] arr) {
		int maxIndex = arr.length-1;
		int minIndex = 0;
		for(int i = 0; i< arr.length; i++) {
			if(i%2 == 0) {
				arr[i] = arr[i] + (arr[maxIndex]%10) * 10;
				maxIndex--;
			} else {
				arr[i] = arr[i] + (arr[minIndex]%10) * 10;
				minIndex++;
			}
		}
		for(int j = 0; j< arr.length; j++) {
			arr[j] = arr[j]/10; 
		}
	}

}
