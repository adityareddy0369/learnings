package problems;

import java.util.LinkedList;
import java.util.Queue;

public class StackUsingTwoQueues {
	
	Queue q1;
	Queue q2;
	
	StackUsingTwoQueues() {
		q1 = new LinkedList<>();
		q2 = new LinkedList<>();
	}

	public static void main(String[] args) {
		StackUsingTwoQueues stack = new StackUsingTwoQueues();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		//System.out.println(stack.pop());
		System.out.println(stack.peek());
		System.out.println(stack.isEmpty());
	}

	private boolean isEmpty() {
		return q1.isEmpty();
	}

	private Object peek() {
		return q1.peek();
	}

	private Object pop() {
		return q1.poll();
	}

	private void push(int x) {
		if(q1.isEmpty()) q1.add(x);
		else {
			while(q1.size() > 0) q2.add(q1.poll());
			q1.add(x);
			while(q2.size() > 0) q1.add(q2.poll());
		}
	}
}
