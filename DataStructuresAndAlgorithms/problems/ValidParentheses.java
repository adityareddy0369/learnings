package problems;

import java.util.Stack;

public class ValidParentheses {

	public static void main(String[] args) {
		String str = "{()[]}";
		System.out.println(isvalid(str));
	}

	private static boolean isvalid(String str) {
		Stack<Character> stack = new Stack<>();
		for(char c: str.toCharArray()) {
			if(c == '(' || c == '{' || c == '[') {
				stack.push(c);
			} else {
				if(stack.empty()) {
					return false;
				} else {
					char top = stack.pop();
					if(c == ')' && top == '(' || c == '}' && top == '{' || c == ']' && top == '[') {
						continue;
					} else {
						return false;
					}
				}
			}
		}
		return stack.empty();
	}

}
